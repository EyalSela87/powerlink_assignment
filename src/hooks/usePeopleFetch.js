import { useState, useEffect } from "react";
import axios from "axios";
import { ContactsOutlined } from "@material-ui/icons";

export const usePeopleFetch = () =>
{
  const [users, setUsers] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [fetchedPage, setFetchedPage] = useState(1);

  // let currentPage = 1;

  useEffect(() =>
  {
    if (fetchedPage === 1)
      fetchUsers();
    else fetchNextpage();
  }, [fetchedPage]);

  async function fetchUsers()
  {
    setIsLoading(true);
    const response = await axios.get(`https://randomuser.me/api/?results=25&page=1`);
    setIsLoading(false);
    setUsers(response.data.results);
  }

  async function fetchNextpage()
  {
    // set loading to true
    setIsLoading(true);
    // get the next page from api
    const response = await axios.get(`https://randomuser.me/api/?results=25&page=${fetchedPage}`);
    // combine the old user array with the received user array
    const currentUsers = [...users];
    const updatedUsers = currentUsers.concat(response.data.results);
    // set the updated user state array
    setUsers(updatedUsers);
    // set loading to false
    setIsLoading(false);
  }

  return { users, isLoading, fetchUsers, fetchedPage, setFetchedPage };
};
