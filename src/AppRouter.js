import React, { useState } from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import { Home } from "pages";
import { ThemeProvider } from "theme";
import NavBar from "components/NavBar";

const AppRouter = () =>
{
  const [tabIndex, setTabIndex] = useState(0);

  const onTabChange = (tabIndex) => setTabIndex(tabIndex)


  return (
    <ThemeProvider>
      <Router>
        <NavBar setTabIndex={(index) => onTabChange(index)} value={tabIndex} />
        <Switch>
          <Route exact path="/" render={() => <Home tabIndex={tabIndex} />} />
        </Switch>
      </Router>
    </ThemeProvider>
  );
};

export default AppRouter;
