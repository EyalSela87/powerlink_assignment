import React, { useEffect, useState, useRef } from "react";
import Text from "components/Text";
import UserList from "components/UserList";
import { usePeopleFetch } from "hooks";
import * as S from "./style";
import { ContactsOutlined } from "@material-ui/icons";

const Home = ({ tabIndex }) =>
{
  const FAVORITE_USER_KEY = "FAVORITE_USER_KEY";

  const { users, isLoading, fetchedPage, setFetchedPage } = usePeopleFetch();
  const [favUsers, setFavUsers] = useState([]);
  const [relevantUsers, setRelevantUsers] = useState([]);

  const prevFavState = useRef();

  useEffect(() =>
  {
    // get the fav users from the storage
    updateUsersFromStorage();
    // get the relevant users array
    checkRelevantUsers(tabIndex === 1)
  }, [tabIndex, users, favUsers]);

  const updateUsersFromStorage = () =>
  {
    if (!localStorage.getItem(FAVORITE_USER_KEY)) return;
    const storageUsers = JSON.parse(localStorage.getItem(FAVORITE_USER_KEY));

    if (shouldUpdateFromStorage(storageUsers))
      setFavUsers(storageUsers)
  }

  const shouldUpdateFromStorage = (storageUsers) =>
  {
    prevFavState.current = favUsers;

    if (storageUsers.length !== prevFavState.current.length) return true;

    for (let i = 0; i < storageUsers.length; i++)
      if (storageUsers[i] !== prevFavState.current[i])
        return true;

    return false;
  }

  const checkRelevantUsers = (onFavTab) =>
  {
    // if on home tab get all the users
    if (!onFavTab)
      setRelevantUsers(users);
    // else get only the favorite users
    else 
    {
      const revUsers = users.filter((user) => favUsers.includes(user.email))
      setRelevantUsers(revUsers);
    }
  }

  const onFavIconClick = (userEmail) =>
  {
    //get favorite users array
    const updatedFavUsers = [...favUsers];
    // get the selected user index from array
    const userIndex = updatedFavUsers.findIndex(fav => fav === userEmail);
    // add/remove the selected/unselected user index
    if (userIndex < 0) updatedFavUsers.push(userEmail);
    else updatedFavUsers.splice(userIndex, 1);
    // update the favorite users state array
    setFavUsers(updatedFavUsers);
    localStorage.setItem(FAVORITE_USER_KEY, JSON.stringify(updatedFavUsers));
  };

  const isFavUser = (user) => favUsers.includes(user);




  const getNextPage = () =>
  {
    // if is on loading or on favorite tab return
    if (isLoading || tabIndex === 1) return;
    // fetch users for the next page
    const nextPage = fetchedPage + 1;
    setFetchedPage(nextPage);
  }


  return (
    <S.Home>
      <S.Content>
        <S.Header>
          <Text size="64px" bold>
            PplFinder
          </Text>
        </S.Header>
        <UserList
          getNextPage={getNextPage}
          users={relevantUsers}
          isLoading={isLoading}
          onFavIconClick={(userIndex) => onFavIconClick(userIndex)}
          isFavUser={(index) => isFavUser(index)} />
      </S.Content>
    </S.Home>
  );
};

export default Home;
