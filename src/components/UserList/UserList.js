import React, { useEffect, useState } from "react";
import Text from "components/Text";
import Spinner from "components/Spinner";
import CheckBox from "components/CheckBox";
import IconButton from "@material-ui/core/IconButton";
import FavoriteIcon from "@material-ui/icons/Favorite";
import * as S from "./style";
import { ContactsOutlined } from "@material-ui/icons";

const UserList = ({ users, isLoading, isFavUser, onFavIconClick, getNextPage }) =>
{
  const [hoveredUserId, setHoveredUserId] = useState();
  const [selectedUsers, setSelectedUsers] = useState(users);
  const [selectedNat, setSelectedNat] = useState([]);

  useEffect(() =>
  {
    manageActiveUsers(selectedNat);
  }, [users]);



  const handleMouseEnter = (index) =>
  {
    setHoveredUserId(index);
  };

  const handleMouseLeave = () =>
  {
    setHoveredUserId();
  };

  const onCheboxChange = (stateNat) =>
  {
    // manage and received the selected nation state array
    const updatedSelectedNat = manageSelectedNat(stateNat);
    // manage active users state array
    manageActiveUsers(updatedSelectedNat);

  };

  const manageSelectedNat = (stateNat) =>
  {
    //get the nation index
    const natIndex = selectedNat.findIndex(nat => nat === stateNat);
    // get nation array from state
    const updatedNatArr = [...selectedNat];
    // add or remove the selected/unselected nation
    if (natIndex < 0) updatedNatArr.push(stateNat);
    else updatedNatArr.splice(natIndex, 1);
    // update nation array state
    setSelectedNat(updatedNatArr);
    // return updated nation array
    return updatedNatArr;
  };

  const manageActiveUsers = (nationsArray) =>
  {
    // active users array
    let activeUsers;
    // if no nation selected set all users as active users
    if (nationsArray.length <= 0)
      activeUsers = users;
    // else check which user having same nation as the selected nations array
    else
    {
      activeUsers = []
      nationsArray.map(nation => { users.map(u => { if (u.nat === nation) activeUsers.push(u); }) })
    }
    // set the active users at state array
    setSelectedUsers(activeUsers);
  };

  const onSCroll = (target) =>
  {
    // get the distance for alomost at  bottom
    const nearlyEndVal = 800;
    const loadPoint = target.scrollHeight - nearlyEndVal;
    // if user almost scroll to the bottom get the next page
    if (target.scrollTop >= loadPoint)
      getNextPage();
  }


  return (
    <S.UserList >
      <S.Filters>
        <CheckBox value="BR" label="Brazil" onChange={(value) => onCheboxChange(value)} />
        <CheckBox value="AU" label="Australia" onChange={(value) => onCheboxChange(value)} />
        <CheckBox value="CA" label="Canada" onChange={(value) => onCheboxChange(value)} />
        <CheckBox value="DE" label="Germany" onChange={(value) => onCheboxChange(value)} />
        <CheckBox value="IE" label="Ireland" onChange={(value) => onCheboxChange(value)} />
        <CheckBox value="DK" label="Denmark" onChange={(value) => onCheboxChange(value)} />
      </S.Filters>
      <S.List onScroll={(event) => onSCroll(event.target)}>
        {selectedUsers.map((user, index) =>
        {
          const isFav = isFavUser(user.email);

          return (
            <S.User
              key={index}
              onMouseEnter={() => handleMouseEnter(index)}
              onMouseLeave={handleMouseLeave}

            >
              <S.UserPicture src={user?.picture.large} alt="" />
              <S.UserInfo>
                <Text size="22px" bold>
                  {user?.name.title} {user?.name.first} {user?.name.last}
                </Text>
                <Text size="14px">{user?.email}</Text>
                <Text size="14px">
                  {user?.location.street.number} {user?.location.street.name}
                </Text>
                <Text size="14px">
                  {user?.location.city} {user?.location.country}
                </Text>
              </S.UserInfo>
              <S.IconButtonWrapper isVisible={index === hoveredUserId || isFav}>
                <IconButton onClick={() => onFavIconClick(user.email)}>
                  <FavoriteIcon color="error" />
                </IconButton>
              </S.IconButtonWrapper>
            </S.User>
          );
        })}
        {isLoading && (
          <S.SpinnerWrapper>
            <Spinner color="primary" size="45px" thickness={6} variant="indeterminate" />
          </S.SpinnerWrapper>
        )}
      </S.List>
    </S.UserList>
  );
};

export default UserList;
